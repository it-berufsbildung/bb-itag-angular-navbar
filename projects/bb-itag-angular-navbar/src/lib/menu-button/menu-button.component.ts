import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IBaseMenuInfo, IMenuItem } from '../model';

@Component({
  selector: 'lib-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.css']
})
export class MenuButtonComponent {

  @Input()
  public menuItem: IMenuItem;

  @Input()
  public location: 'top' | 'left' = 'top';

  @Input()
  public routerLinkActiveClass = 'active';

  @Output()
  public menuClicked: EventEmitter<IBaseMenuInfo> = new EventEmitter<IBaseMenuInfo>();

  public onMenuClicked() {
    this.menuClicked.emit(this.menuItem);
  }

  public getActiveRouterLink() {
    if (! this.menuItem) { return ''; }
    if (! this.menuItem.routerLink || this.menuItem.routerLink.length === 0) { return ''; }
    if ( this.routerLinkActiveClass && this.routerLinkActiveClass.length > 0 ) { return this.routerLinkActiveClass; }
    return '';
  }

}
