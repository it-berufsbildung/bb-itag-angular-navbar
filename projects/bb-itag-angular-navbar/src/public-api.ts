/*
 * Public API Surface of bb-itag-angular-navbar
 */

export { IBaseMenuInfo, IBaseMenuItem, IMenuConfiguration, IMenuItem, IProfileMenuItem, IsMenuVisibleMethod, SizeProp } from './lib/model';
export { BbItagAngularNavbarComponent } from './lib/bb-itag-angular-navbar.component';
export { BbItagAngularNavbarModule } from './lib/bb-itag-angular-navbar.module';

