import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';
import { BbItagAngularNavbarModule } from '../../projects/bb-itag-angular-navbar/src/public-api';
import { RoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { MatButtonModule, MatSlideToggleModule, MatTooltipModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RoutingModule,
        RouterModule,
        FontAwesomeModule,
        BbItagAngularNavbarModule,
        MatButtonModule,
        MatTooltipModule,
        MatSlideToggleModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
