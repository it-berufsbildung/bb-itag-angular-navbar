import { Observable } from 'rxjs';
import { IMenuItem } from './IMenuItem';

export type IsMenuVisibleMethod = (
  menuItem: IMenuItem,
  ) => Observable<boolean>;
