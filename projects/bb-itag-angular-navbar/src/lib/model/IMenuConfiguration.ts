import { ThemePalette } from '@angular/material/core';
import { IMenuItem } from './IMenuItem';
import { IProfileMenuItem } from './IProfileMenuItem';
import { IsMenuVisibleMethod } from './checkIsVisibleType';

export interface IMenuConfiguration {
  // format of the method
  isMenuVisibleMethod: IsMenuVisibleMethod;
  // default theme ist primary
  color: ThemePalette;
  // the array with the menus, showing left
  leftMenuItems: IMenuItem[];
  // the array with the menus, showing right
  rightMenuItems: IMenuItem[];
  // the name we show on the top of the menu (default: myApp)
  applicationName: string;
  // we strongly recommend to add a alternate name here in case you have an uri set (default: the logo of the company)
  applicationLogoAlt: string;
  // the logo to the image we show on the top left of the menu (recommended size: 120x30px), default is null
  applicationLogoUri: string;
  // how we should show the profile menu (not show by default)
  profile: IProfileMenuItem;
  // what global css class we have to use to show the active menu
  activeRouterLinkClass: string;
  // do we have to log some debug information like menu click, or isVisible checks
  showDebug: boolean;
  // the router link on the application name / logo on the left. default /home
  applicationRouterLink: string;
}
