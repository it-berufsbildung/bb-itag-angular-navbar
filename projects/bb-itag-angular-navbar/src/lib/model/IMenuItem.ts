import { IBaseMenuItem } from '.';

export type SizeProp =
  | 'xs'
  | 'lg'
  | 'sm'
  | '1x'
  | '2x'
  | '3x'
  | '4x'
  | '5x'
  | '6x'
  | '7x'
  | '8x'
  | '9x'
  | '10x'
  ;

export interface IMenuItem extends IBaseMenuItem {
  roles: string[];  // '.' indicates that you have to be authenticated, or you add in the array the list of roles
  childMenuItems?: IMenuItem[];
}
