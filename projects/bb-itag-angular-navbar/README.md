# bb-itag-angular-navbar Library

This is an Angular-Navbar Library, which gives you a ready-made navbar. It is possible to customize the navbar with attributes. The navbar is collapsing on small screens for mobile applications.
This ReadMe explains how to integrate the navbar and how to configure it.
![navbar demo page]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/demo-menu.png "Navbar Demo Page") 

### Dependencies

The navbar requires Angular Material, FlexLayout and FontAwesome.

These can all be integrated via the terminal:
* Angular Material: 
```
ng add @angular/material
```
* FontAwesome: 
```
npm i -s @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome@0.5.x
```
* FlexLayout:
```
npm i -s @angular/flex-layout @angular/cdk
```


### Installation

install the library via NPM:
```text
npm i -s bb-itag-angular-navbar
```

shortcut with all dependencies together:
```
npm i -s @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/angular-fontawesome@0.5.x @angular/flex-layout @angular/cdk bb-itag-angular-navbar
```


### Using the library in the application

To use the library in your application, add to the app.module ts the following import: 
``` typescript
import { BbItagAngularNavbarModule } from 'bb-itag-angular-navbar'

// add the navbar module in the module imports
@NgModule({
  imports:
    BbItagAngularNavbarModule
})

```

Now it's time to configure the menu.

add the navbar to your application. We recommend to create your own mainApp component but you can use it direct in the app-compoment files.

###### app.component.html 
```html
<div>
  <lib-bb-itag-angular-navbar></lib-bb-itag-angular-navbar>
  <router-outlet ></router-outlet>
</div>
```

This will show you a simple bar on the top with the applicationName on the left and the user TEST with an icon on the right.

![navbar default menu]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/demo-menu.png "Navbar default configuration view")
![navbar default menu]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/default-menu-small.png "Navbar default configuration view small screen")
![navbar default menu]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/default-menu-small-open.png "Navbar default configuration view small screen open menu")

#### Menu Configuration

We recommend that you outsource the menu configuration in a separate file but you can do it for sure all in the same file.

1. First of all, we are gone create a typescript file. We simply call it "menuConfiguration.ts". We store it usually on the same place as we store the component where we use the configurations file, but it can be anywhere in your project.
This file is basically to create the menu on the left and on the right.
###### menuconfiguration.ts
```typescript
import { faGrin, faSmile, faToolbox } from '@fortawesome/free-solid-svg-icons';
import { IMenuItem, IProfileMenuItem } from '../../projects/bb-itag-angular-navbar/src/lib/model';

const submenu1: IMenuItem = {
  name: 'submenu 1',
  routerLink: '/authenticated/submenu1',
  roles: ['isUser1'],
  faIcon: faGrin,
};
const submenu2: IMenuItem = {
  name: 'submenu 2',
  routerLink: '/authenticated/submenu2',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
};
const submenu3: IMenuItem = {
  name: 'submenu 3',
  routerLink: '/authenticated/submenu3',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
};
const menuItemAuthentication: IMenuItem = {
  name: 'Authenticated',
  roles: ['.'],
  childMenuItems: [
    submenu1,
    submenu2,
    submenu3
  ]
};

const spacerIsAuthenticated: IMenuItem = {
  name: '-',
  roles: ['.'],
};
const spacerIsAdmin: IMenuItem = {
  name: '-',
  roles: ['isAdmin'],
};
const spacerIsUser1: IMenuItem = {
  name: '-',
  roles: ['isUser1'],
};
const spacerIsUser2: IMenuItem = {
  name: '-',
  roles: ['isUser2'],
};

const menuItemSub2: IMenuItem = {
  name: 'Sub2',
  faIcon: faToolbox,
  roles: ['.'],
  childMenuItems: [
    submenu1,
    spacerIsUser1,
    submenu2,
    spacerIsUser2,
    submenu3
  ]
};

const menuItemAdmin: IMenuItem = {
  name: 'Admins',
  routerLink: '/admin',
  roles: ['isAdmin'],
  faIcon: faToolbox,
  faSize: '1x'
};

export const leftMenuItems: IMenuItem[] = [
  menuItemAuthentication,
  submenu1,
  submenu2,
  submenu3,
  menuItemSub2,
];

export const rightMenuItems: IMenuItem[] = [
  menuItemAdmin,
];

```

2. In your component.ts (like app.component.ts) file we can now make your settings. A few values must be passed. It can look like this in the beginning.
there is an IMenuConfiration interface definition available with all the possible settings.
###### app.component.ts
```typescript

/* tslint:disable:member-ordering */
import { Component } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { Observable, of } from 'rxjs';
import { IMenuItem, IProfileMenuItem, IsMenuVisibleMethod } from 'bb-itag-angular-navbar';
import { leftMenuItems, rightMenuItems } from './menuConfiguration';

// @ts-ignore
import * as pjson from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private isAuthenticatedTrueMethod (): Observable<boolean> {
    console.log ( `isAuthenticatedTrueMethod` );
    return of ( true );
  }

  private static isMenuCheckedMethod( menuItem: IMenuItem ): Observable<boolean> {
    console.log(menuItem.roles);
    return of (
      menuItem.roles.indexOf ( 'isUser2' ) > -1
      || menuItem.roles.indexOf ( 'isAdmin' ) > -1
      || menuItem.roles.indexOf ( '.' ) > -1
    );
  }

  // @ts-ignore
  private profileMenuItem: IProfileMenuItem = {
    showProfile: true,
    routerLink: '/profile',
    // faIcon: faSmile,
    // faSize: '2x'
    username$: of ( 'test' ),
    isAuthenticated$: this.isAuthenticatedTrueMethod(),
  };

  // the material color settings warn, accent or primary (default 'primary')
  public color: ThemePalette = 'primary';
  // the array with the menus, showing left (default [])
  public leftMenuItems: IMenuItem[] = leftMenuItems;
  // the array with the menus, showing right (default [])
  public rightMenuItems: IMenuItem[] = rightMenuItems;
  // the name we show on the top of the menu (default: 'myApp')
  public applicationName: string = pjson.name;
  // we strongly recommend to add a alternate name here in case you have an uri set (default: 'the logo of the company')
  public applicationLogoAlt: string = 'the alternate name for the logo';
  // the logo to the image we show on the top left of the menu (recommended size: 120x30px) ) (default: null)
  public applicationLogoUri: string = '/assets/images/logo.png';
  // how we should show the profile menu (default: username$ = of('test'), isAuthenticated$ = of(true), showProfile = true)
  public profile: IProfileMenuItem = this.profileMenuItem;
  // what global css class we have to use to show the active menu (default: 'empty', this is an internal class)
  public activeRouterLinkClass: string = 'active';
  // do we have to log some debug information like menu click, or isVisible checks (default: false)
  public showDebug: boolean = false;
  // the router link on the application name / logo on the left. (default /)
  public applicationRouterLink: string = null;
  // set the property to the method to call (default to a internal method that returns always of(true))
  public isMenuVisibleMethod: IsMenuVisibleMethod = AppComponent.isMenuCheckedMethod;
}
```

3. Then we refer in app.component.html to the config file settings. All parameter have a default value 
###### app.component.html 
```html
<div>
  <lib-bb-itag-angular-navbar
    [isMenuVisibleMethod]="isMenuVisibleMethod"
    [color]="color"
    [leftMenuItems]="leftMenuItems"
    [rightMenuItems]="rightMenuItems"
    [applicationName]="applicationName"
    [applicationLogoAlt]="applicationLogoAlt"
    [applicationLogoUri]="applicationLogoUri"
    [profile]="profile"
    [activeRouterLinkClass]="activeRouterLinkClass"
    [showDebug]="showDebug"
    [applicationRouterLink]="applicationRouterLink"
  ></lib-bb-itag-angular-navbar>
  <router-outlet ></router-outlet>
</div>
```

Now the navbar should be displayed with the contents of menuConfiguration.ts

![navbar demo menu]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/demo-menu.png "Navbar demo view")
![navbar demo menu]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/demo-menu-small.png "Navbar demo view small")
![navbar demo menu]( https://gitlab.com/bb-itag/bb-itag-angular-navbar/raw/master/projects/bb-itag-angular-navbar/src/images/demo-menu-small-open.png "Navbar demo view small open")


### explanation of the attributes

#### IsMenuVisibleMethod
* this method allows you to check if the navbar should show the menu or not. Usually you have some authentication layer with that you can check if the user is in the right role to access the menu. The navbar handover to you the IMenuItem with the roles so you can check it.

###### datatype
```typescript
export type IsMenuVisibleMethod = (
    menuItem: IMenuItem,
    ) => Observable<boolean>;
```

###### default
internal method that always returns of(true)
```typescript
import { IMenuItem, IProfileMenuItem, IsMenuVisibleMethod } from 'bb-itag-angular-navbar';

private isMenuVisibleTrueMethod(menuItem: IMenuItem): Observable<boolean> {
    if ( this.menuConfiguration.showDebug ) {
      console.log ( `bb-itag-angular-navbar-component.isMenuVisibleTrueMethod.menuItem: ${ JSON.stringify ( menuItem, null, 2 ) }` );
    }
    return of ( true );
}
```

#### color
Angular material ThemePalette

###### datatype
```typescript
import { ThemePalette } from '@angular/material/core';
```
###### default
````typescript
'primary'
````
#### leftMenuItems or rightMenuItems
we split the navigation bar on top in two general parts. left and right
usually you have on the left application relevant menu points and on the right more adminstrative menu points. But you can have either all your menu items left or right as well, just set an empty array instead or don't set as the empty array is the default.

###### datatype
```typescript
const menuItemAdmin: IMenuItem = {
  name: 'Admins',
  routerLink: '/admin',
  roles: ['isAdmin'],
  faIcon: faToolbox,
  faSize: '1x'
};

export const rightMenuItems: IMenuItem[] = [
  menuItemAdmin,
];
```
###### default
````typescript
[]
````
#### applicationName
If you want to set the application name in the navbar. If you don't like to have an application name you must set it to NULL as we useing the default myApp

```typescript
applicationName: 'yourAppName'
```
or if you take it from the package.json file 
```typescript
import pjson from '../../package.json';

applicationName: pjson.name
```

###### datatype
```typescript
string
```


###### default
````typescript
'myApp'
````
#### applicationLogoAlt
you always should set an logo alternate description as this is the only way disabled people can understand what is behind the image.

###### datatype
```typescript
string
```
###### default
````typescript
'the logo of the company'
````
#### applicationLogoUri
In the menu configuration the path should be specified at applicationLogoUri, where the logo is located. 
The example config has the logo located in this path: assets/images/logo.png.

With the value `null` for applicationLogoUri no logo gets displayed.

###### datatype
```typescript
string
```
###### default
````typescript
null
````
#### profile
the profile is a special menu on the fare right. If you don't set it nothing will show.
If your application has authentication and you like to see the username and if you click on it you like to move to a specific page you have to set this attribute. see also on the datatype description at the end of the file!

if we have a profile set you have to set the username$ and isAuthenticated$ observable.

```typescript
     // the username$ is an observable that you can easy use to add it to an userservice
     // the isAuthenticated$ is an observable that allows you to set the status accordingly (usually you use it with an userservice)
     profile.username$ = of('aName');
     isAuthenticated$ = of(true);
  }
```

###### datatype
```typescript
import { IProfileMenuItem } from 'bb-itag-angular-navbar';

```
###### default
````typescript
null
````
#### activeRouterLinkClass
The navbar allows you to set the class of the current active router link. 
as we could not find a proper way to handover a full class as configuration we decicde to use a global css class (sorry for that)

1. Create a class in your theme.scss or styles.scss so it is global.
Example: 
```css
activeRouterLinkColor { color: orange; }
```
2. Set the class in your menuConfiguration in the menuConfig.ts file: 
```typescript
activeRouterLinkClass: '[name of the class]'
```

If you don't want any color set for the current active router link, then just set the class: `activeRouterLinkClass: 'empty'` or keep the default
###### datatype
```typescript
string
```
###### default
````typescript
// do not override this empty class for the menu, instead of override create a new one
'empty'

````
#### showDebug
if we can, we show debug information on the console. Most used for our own interal testing but we keep it inside

###### datatype
```typescript
boolean
```
###### default
````typescript
false
````
#### applicationRouterLink
By default we redirect to the / route in case you click on the applicationName or the applicationLogo. but you can choose by setting this attribute to a route on your wish

```typescript
applicationRouterLink="/home"
```

###### datatype
```typescript
string
```
###### default
````typescript
'/'
````

### dataTypes and interfaces

#### IMenuItem

* name: the text to show
* roles: an array that contains all required roles. We do have the following ruling for this array
    * [] --> everybody has access, no checkes will be done
    * [.] --> only authenticated users will be see this menu. Independent of the roles
    * ['anyRoleName you like'] --> we ask you over the isMenuVisibleMethod if the navbar should show the menu or not
* faIcon: the faIconDefinition. As you may use differnt fantAwesome libraries we decide to go for any datatype, default NULL
* faSze: the size of the icon (default 1x)
* routerLink: the link we have to call if the user press the menuitem, default NULL
* childMenuItems: only available on the top level. Only one level of children is supported. default NULL

```typescript
export interface IMenuItem {
  name: string;
  roles: string[];
  faIcon?: any;
  faSize?: SizeProp;
  routerLink?: string;
  childMenuItems?: IMenuItem[];
}
```
example:
Have a look on the spaceIsAuthenticated. This is also a very special IMenuItem. If you use a - as name we assume it's a spacer and we draw an spacer on that spot.
```typescript
const spacerIsAuthenticated: IMenuItem = {
  name: '-',
  roles: ['.'],
};
const submenu1: IMenuItem = {
  name: 'submenu 1',
  routerLink: '/submenu1',
  roles: ['isUser1'],
  faIcon: faGrin,
};
const submenu2: IMenuItem = {
  name: 'submenu 2',
  routerLink: '/submenu2',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
};
const submenu3: IMenuItem = {
  name: 'submenu 3',
  routerLink: '/submenu3',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
};
const menuItemAuthentication: IMenuItem = {
  name: 'Authenticated',
  roles: ['.'],
  childMenuItems: [
    submenu1,
    spacerIsAuthenticated,
    submenu2,
    submenu3
  ]
};
```

### faIcon
If you want a different font Awesome icon than the already existing one then you have to
 add it to the import and then in the property
```typescript 
import { faSmile } from '@fortawesome/free-solid-svg-icons';

menuItem.faIcon = faSmile;
```

#### SizeProp
In general it's the same size type as fontAwesome supports.

```typescript
export type SizeProp =
  | 'xs'
  | 'lg'
  | 'sm'
  | '1x'
  | '2x'
  | '3x'
  | '4x'
  | '5x'
  | '6x'
  | '7x'
  | '8x'
  | '9x'
  | '10x';


menuItem.faSize = '1x';

```

#### IProfileMenuItem

* showProfile: you can hide or show the profile either by not using the profile at all or by switching the showProfile to false. Default is true
* routerLink: the routerlink in case the user is clicking on the username or the usericon. default null
* username$: the username to show. made as observable so you can easy use an authentication service
* isAuthenticated: the observable that indicates if the user is logged in or not. also build as observable to allow an authentication service take control over it
* faIcon: the faIconDefinition. As you may use differnt fantAwesome libraries we decide to go for any datatype, default faUser
* faSze: the size of the icon (default 1x)
```typescript
import { Observable } from 'rxjs';
import { SizeProp } from './IMenuItem';

export interface IProfileMenuItem {
  showProfile: boolean;
  routerLink?: string;
  username$: Observable<string>;
  isAuthenticated$: Observable<boolean>;
  faIcon?: any;
  faSize?: SizeProp;
}
```

#### IsMenuVisibleMethod

```typescript
import { Observable } from 'rxjs';
import { IMenuItem } from 'bb-itag-angular-navbar';

export type IsMenuVisibleMethod = (
  menuItem: IMenuItem,
  ) => Observable<boolean>;
```

an example can be like this:
```typescript
isMenuVisibleMethod ( menuItem: IMenuItem ): Observable<boolean> {
    // console.log(menuItem.roles);
    return of(
      menuItem.roles.indexOf('isUser2') > -1
      || menuItem.roles.indexOf('isAdmin') > -1
      || menuItem.roles.indexOf('.') > -1
    );
  };
  
  // and using the code
  yourMenuIsVisibleMethod = isMenuVisibleMethod; // YES, without brackets here as we handover the handle to the method
```

#### IMenuConfiguration
contains all possible settings for internal use only but you may can see all possible options here:

```typescript
import { IMenuItem, IProfileMenuItem, IsMenuVisibleMethod } from 'bb-itag-angular-navbar';

export interface IMenuConfiguration {
  // format of the method
  isMenuVisibleMethod: IsMenuVisibleMethod;
  // default theme ist primary
  color: ThemePalette;
  // the array with the menus, showing left
  leftMenuItems: IMenuItem[];
  // the array with the menus, showing right
  rightMenuItems: IMenuItem[];
  // the name we show on the top of the menu (default: myApp)
  applicationName: string;
  // we strongly recommend to add a alternate name here in case you have an uri set (default: the logo of the company)
  applicationLogoAlt: string;
  // the logo to the image we show on the top left of the menu (recommended size: 120x30px)
  applicationLogoUri: string;
  // how we should show the profile menu (not show by default)
  profile: IProfileMenuItem;
  // what global css class we have to use to show the active menu
  activeRouterLinkClass: string;
  // do we have to log some debug information like menu click, or isVisible checks
  showDebug: boolean;
  // the router link on the application name / logo on the left. default /home
  applicationRouterLink: string;
```
