import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, UrlSegment } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public urlInfo: string;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) {
    activatedRoute.url
      .subscribe( (value: UrlSegment[]) => {
        this.urlInfo = value.join('-').toString();
      });
  }

  ngOnInit() {
  }

}
